﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Timers;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Excel = Microsoft.Office.Interop.Excel;

namespace WpfLab2_Mutiuk
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region peremennie
        Random x = new Random();
        Random y = new Random();
        public Point p = new Point();
        public Point[] pi = new Point[2];
        int Status_exp = 0;
        DateTime Start;
        DateTime Stopped;
        TimeSpan Elapsed = new TimeSpan();
        public Point s;
        #endregion

        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            lbRez.Items.Clear();
            lbRez.Items.Add("Результат");
            expir();
        }

        private Point DrowObject(Point p)
        {
            Rectangle el = new Rectangle();
            el.Width = 8;
            el.Height = 8;
            p.X = x.Next(Convert.ToInt32(this.Width - el.Width));
            p.Y = y.Next(Convert.ToInt32(icDow.ActualHeight - el.Height));
            el.Fill = Brushes.Black;
            InkCanvas.SetLeft(el, p.X);
            InkCanvas.SetTop(el, p.Y);
            icDow.Children.Add(el);
            return p;
        }

        public void expir()
        {
            int[] rez_arr = new int[2];
            Array.Clear(pi, 0, pi.Length - 1);
            Start = new DateTime(0);
            icDow.Children.Clear();
            icDow.Strokes.Clear();
            for (int i = 0; i < 2; i++)
            {
                pi[i] = DrowObject(p);
            }
            Status_exp = Status_exp + 1;
        }

        private void icUp(object sender, MouseButtonEventArgs e)
        {
            if (Status_exp > 0)
            {
                Point[] n = Array.FindAll(pi, element => (Math.Abs(element.X - e.GetPosition(this).X) < 20) && (Math.Abs(element.Y - e.GetPosition(this).Y) < 20));
                if (Array.Exists(pi, element => (Math.Abs(element.X - e.GetPosition(this).X) < 20) && (Math.Abs(element.Y - e.GetPosition(this).Y) < 20)))
                {
                    if (!((Math.Abs(s.X - e.GetPosition(this).X) < 10) && (Math.Abs(s.Y - e.GetPosition(this).Y) < 10)))
                    {
                        s = e.GetPosition(this);
                        {
                            if (Start.Ticks == 0)
                            {
                                Start = DateTime.Now;
                            }
                            else
                            {
                                Stopped = DateTime.Now;
                                Elapsed = Stopped.Subtract(Start);
                                long elapsedTicks = Stopped.Ticks - Start.Ticks;
                                TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);
                                double rez = Math.Sqrt(Math.Pow(pi[0].X - pi[1].X, 2) + Math.Pow(pi[0].Y - pi[1].Y, 2));
                                lbRez.Items.Add("Експеримент " + Status_exp.ToString() + "\n" + "Час: " + elapsedSpan.Milliseconds.ToString() + "\n" + "Відстань " + rez + "\n");


                                Excel.Application ex = new Excel.Application();
                                try
                                {
                                    Excel.Workbook workBook = ex.Workbooks.Open(@"E:\Людино-машинний інерфейс\WpfLab2_Mutiuk\lab2_1.xlsx");
                                }
                                catch
                                {
                                    Excel.Workbook workBook = ex.Workbooks.Add(Type.Missing);
                                    ex.Application.ActiveWorkbook.SaveAs(@"E:\Людино-машинний інерфейс\WpfLab2_Mutiuk\lab2_1.xlsx");
                                    ex.Application.ActiveWorkbook.Close();
                                    workBook = ex.Workbooks.Open(@"E:\Людино-машинний інерфейс\WpfLab2_Mutiuk\lab2_1.xlsx");
                                }

                                Excel.Worksheet sheet = (Excel.Worksheet)ex.Worksheets.get_Item(1);
                                sheet.Name = "Лабораторна робота 2";
                                sheet.Cells[Status_exp + 1, 1] = String.Format("{0}", Status_exp.ToString());
                                sheet.Cells[Status_exp + 1, 2] = String.Format("{0}", elapsedSpan.Milliseconds.ToString());
                                sheet.Cells[Status_exp + 1, 3] = String.Format("{0}", Math.Round(rez));
                                ex.Application.ActiveWorkbook.Save();
                                ex.Application.ActiveWorkbook.Close();




                                if (Status_exp < 100)
                                    expir();
                            }
                        }
                    }
                }
            }
        }



    }
}
